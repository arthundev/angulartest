export class Product {
    id: string;
    name: string;
    feature: string;
    country: string;
}