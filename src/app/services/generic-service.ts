import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class GenericService {
    constructor(private http: HttpClient) { }

    public getData(): Observable<any> {
        return this.http.get("./assets/data.json")
    }

    public getCountries(): Observable<any> {
        return this.http.get("https://restcountries.eu/rest/v2/all?fields=name")
            .pipe(map((res: any[]) => {
                return res.map(item => { return { label: item.name, value: item.name } });
            }));
    }
}
